package br.com.services;

import br.com.clients.LogEmpresaValidaClient;
import br.com.clients.ReceitaClient;
import br.com.enums.StatusCadastroEnum;
import br.com.models.Empresa;
import br.com.models.EmpresaReceita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidaEmpresaService {

    @Autowired
    private ReceitaClient receitaClient;

    @Autowired
    private LogEmpresaValidaClient logEmpresaValidaClient;

    public void validaEmpresa(Empresa empresa){
        EmpresaReceita empresaReceita = receitaClient.getEmpresaNaReceitaByCnpj(empresa.getCnpj());

        empresa.setCapitalSocial(empresaReceita.getCapitalSocial());

        if (empresa.getCapitalSocial()>1000000){
            empresa.setStatusCadastro(StatusCadastroEnum.ACEITO);
        }else{
            empresa.setStatusCadastro(StatusCadastroEnum.RECUSADO);
        }

        logEmpresaValidaClient.gerarLogEmpresa(empresa);

        System.out.println(empresa);
    }
}
