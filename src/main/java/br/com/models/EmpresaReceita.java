package br.com.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmpresaReceita {

    @JsonProperty("capital_social")
    private double capitalSocial;

    public double getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(double capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public EmpresaReceita() {
    }

}
