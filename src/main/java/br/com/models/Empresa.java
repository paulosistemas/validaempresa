package br.com.models;

import br.com.enums.StatusCadastroEnum;

import javax.persistence.Entity;
import javax.persistence.Id;


public class Empresa {

    private String nome;

    private String cnpj;

    private double capitalSocial;

    private StatusCadastroEnum statusCadastro;

    public double getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(double capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public StatusCadastroEnum getStatusCadastro() {
        return statusCadastro;
    }

    public void setStatusCadastro(StatusCadastroEnum statusCadastro) {
        this.statusCadastro = statusCadastro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String toString(){
        return "nome: " + nome + " CNPJ: " + cnpj + " Capital social: " + capitalSocial + " Status: " + statusCadastro;
    }

    public Empresa() {
    }
}
