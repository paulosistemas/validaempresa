package br.com.consumers;

import br.com.models.Empresa;
import br.com.services.ValidaEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    ValidaEmpresaService validaEmpresaService;

    @KafkaListener(topics="spec3-paulo-cesar-2" , groupId = "empresa")
    public void receberMensagem(@Payload Empresa empresa){
        System.out.println("Empresa recebida: " + empresa);
        validaEmpresaService.validaEmpresa(empresa);
    }
}
