package br.com.clients;

import br.com.models.Empresa;
import br.com.models.EmpresaReceita;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name="logEmpresa")
public interface LogEmpresaValidaClient {

    @PostMapping("/Empresa/Log")
    EmpresaReceita gerarLogEmpresa(@RequestBody Empresa empresa);
}
