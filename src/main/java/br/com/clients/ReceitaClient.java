package br.com.clients;

import br.com.models.EmpresaReceita;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "receita", url = "https://www.receitaws.com.br/")
public interface ReceitaClient {

    @GetMapping("v1/cnpj/{cnpj}")
    EmpresaReceita getEmpresaNaReceitaByCnpj(@PathVariable String cnpj);

}
