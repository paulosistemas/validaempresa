package br.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EmpresaConsumerApplication {

    public static void main(String[] args){

        SpringApplication.run(EmpresaConsumerApplication.class, args);

    }
}
